package com.example.fabio.sizzoniapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.support.constraint.ConstraintLayout;
import android.util.Log;
import android.view.View;

public class SlidingView extends View {

    private MainActivity main;
    private Bitmap img = null;
    private int imageToDraw;
    private int viewSize;
    private ConstraintLayout innerLayout;
    private int x_img, y_img;


    //private boolean dragOn = false;

    public SlidingView(Context context, int imageToDraw, ConstraintLayout innerLayout) {
        super(context);
        this.imageToDraw = imageToDraw;
        this.innerLayout = innerLayout;
    }

    private void localInit(Context context) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inJustDecodeBounds = true;
        if (imageToDraw == 1) {
            x_img = 0;
            y_img = 100;
            img = BitmapFactory.decodeResource(context.getResources(), R.drawable.spike);
        } else if (imageToDraw == 2) {
            x_img = innerLayout.getWidth()-viewSize;
            y_img = 500;
            img = BitmapFactory.decodeResource(context.getResources(), R.drawable.woog);

        } else {
            x_img = 0;
            y_img = 900;
            img = BitmapFactory.decodeResource(context.getResources(), R.drawable.panda);
        }
        viewSize = (int) getResources().getDimension(R.dimen.dime);
        img = Bitmap.createScaledBitmap(img, viewSize, viewSize, true);

    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        localInit(main.getApplicationContext());

        Log.d("PJDM", "x:"+x_img+"   y:"+y_img);
        //altrimenti sto muovendo la pic, quindi devo solo ridisegnarla con i valori già aggiornati in moveToRight o moveToLeft
        canvas.drawBitmap(img, x_img,y_img, null);
    }

    public void setMain(MainActivity main) {
        this.main = main;
    }

    public void moveToRight() {
        int x_to_reach = innerLayout.getWidth() + viewSize;

        while (x_img < x_to_reach) {
            Log.d("PJDM", "in while");
            x_img += 1;
            invalidate();
        }
    }

    public void moveToLeft() {
        int x_to_reach = -viewSize;
        while (x_img > x_to_reach) {
            x_img -= 1;
            invalidate();
        }
    }

    public int get_img_x() {
        return x_img;
    }
    public int get_img_y() {
        return y_img;
    }


}

