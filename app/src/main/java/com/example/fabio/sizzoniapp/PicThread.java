package com.example.fabio.sizzoniapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class PicThread extends Thread {
    private int[] pics = {0,0,0};

    private Handler picHandler;

    public PicThread (Handler picHandler) {
        this.picHandler = picHandler;
    }

    @Override
    public void run() {
        pics[0] = (int) (Math.random()*3)+1;

        while ( pics[1] == pics[0] || pics[1]<1) { //scelgo un altro intero per la seconda pic diverso da quello per la prima
            pics[1]=(int) (Math.random()*3)+1;
        }
        while (pics[2] == pics[0] || pics[2] == pics[1] || pics[2]<1) {
            pics[2] = (int) (Math.random()*3)+1;
        }
        sendTheChoice(pics);
    }

    public void sendTheChoice(int[] pics) {
        Message msg = picHandler.obtainMessage();
        Bundle b = new Bundle();
        b.putIntArray("pics", pics);
        msg.setData(b);
        picHandler.sendMessage(msg);
    }
}
