package com.example.fabio.sizzoniapp;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class PicView extends ImageView {

    private float x_start;
    private float y_start;
    private float x_tmp;
    private float y_tmp;
    private boolean dragOn = false;
    private ImageView bin;
    private MainActivity main;

    public PicView(Context context) {
        super(context);
    }

    public PicView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int eventaction = event.getAction();

        int x = (int) event.getX() - this.getWidth()/2; //x di dove clicco
        int y = (int) event.getY() - this.getHeight()/2; //y di dove clicco


        //imposto uno switch che gestisca i tre eventi DOWN, MOVE, UP
        switch(eventaction) {
            case MotionEvent.ACTION_DOWN: //quando clicco sulla textview imposto come selezionato e mi prendo la x e y iniziali
                dragOn = true;

                //mi salvo le posizioni iniziali dell'immagine
                x_start = this.getX();
                y_start = this.getY();

                //salvo le ultime coordinate dell'ultimo evento di click (serve per il drag)
                x_tmp = event.getX();
                y_tmp = event.getY();
                break;
            case MotionEvent.ACTION_MOVE: //muovi la textview, aggiorna le coordinate e ridisegna

                this.setX(this.getX() + x);
                this.setY(this.getY() + y);

                x_tmp = x;
                y_tmp = y;
                break;
            case MotionEvent.ACTION_UP:
                /*int[] bin_pos = new int[2];
                int[] tv_pos = new int[2];
                bin.getLocationInWindow(bin_pos);
                this.getLocationInWindow(tv_pos);

                if (Math.abs(bin_pos[0]-tv_pos[0])<bin.getWidth()/2 && Math.abs(bin_pos[1]-tv_pos[1])<bin.getHeight()/2) {
                    main.increaseCnt();
                }*/

                this.setX(x_start);
                this.setY(y_start);
                dragOn = false;
                break;
        }

        return true;
    }
}
