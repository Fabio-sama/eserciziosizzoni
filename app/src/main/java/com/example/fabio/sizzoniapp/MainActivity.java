package com.example.fabio.sizzoniapp;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ConstraintLayout mainLayout;
    private ConstraintLayout innerLayout;
    private PicView pic1;
    private PicView pic2;
    private PicView pic3;
    private PicThread picThread;
    //private slidingThread[] threads = new slidingThread[3];
    private SlidingView[] views = new SlidingView[3];


    @SuppressLint("HandlerLeak")
    private Handler picHandler = new Handler(){ //creo handler per ricevere messaggi dal thread
        @Override
        public void handleMessage(Message msg) {

            //prendo il primo messaggio che mi ha mandato il thread che si è svegliato e ha preso il lock
            int [] pics = msg.getData().getIntArray("pics");
            int id = getResources().getIdentifier("pic"+(pics[0]+""), "drawable", getPackageName());
            pic1.setImageResource(id);
            id = getResources().getIdentifier("pic"+(pics[1]+""), "drawable", getPackageName());
            pic2.setImageResource(id);
            id = getResources().getIdentifier("pic"+(pics[2]+""), "drawable", getPackageName());
            pic3.setImageResource(id);

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainLayout = findViewById(R.id.mainLayout);
        innerLayout = findViewById(R.id.innerLayout);

        pic1 = findViewById(R.id.pic1);
        pic2 = findViewById(R.id.pic2);
        pic3 = findViewById(R.id.pic3);

        //thread che di volta in volta decide l'ordine delle picview con il cibo. Crea un int[3] random e lo manda alla main
        picThread = new PicThread(picHandler);
        picThread.start();


        //creo le view dei ciccioni che dovranno scorrere e ricevere cibo
        views[0] = new SlidingView(this, 1, innerLayout);
        views[0].setMain(this);

        views[1] = new SlidingView(this, 2, innerLayout);
        views[1].setMain(this);

        views[2] = new SlidingView(this, 3, innerLayout);
        views[2].setMain(this);

        //aggiungo le view dei ciccioni al layout
        innerLayout.addView(views[0]);
        innerLayout.addView(views[1]);
        innerLayout.addView(views[2]);

    }
}
